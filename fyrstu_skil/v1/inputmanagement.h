#ifndef INPUTMANAGEMENT_H
#define INPUTMANAGEMENT_H

#include "scientist.h"
#include "datacontrol.h"

class inputManagement
{
private:
    dataControl data_control;
    vector<Scientist> scientistVector;
public:
    inputManagement();
    void add(Scientist legend);
    void display();
    void saveAndExit();
    bool sortStringAlphabeticallyByName(const Scientist &s1, const Scientist &s2);
    void showSortedNames();
    // friend bool operator== (Point &cP1, Point &cP2);

    void findString(string str);
};

#endif // INPUTMANAGEMENT_H
