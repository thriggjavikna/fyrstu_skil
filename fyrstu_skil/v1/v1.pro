#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T13:57:13
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = v1
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    gui.cpp \
    inputmanagement.cpp \
    datacontrol.cpp \
    scientist.cpp

HEADERS += \
    gui.h \
    inputmanagement.h \
    datacontrol.h \
    scientist.h
