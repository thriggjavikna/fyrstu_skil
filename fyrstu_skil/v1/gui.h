#ifndef GUI_H
#define GUI_H
#include "inputmanagement.h"
#include <string>
#include <iostream>
#include <iterator>
#include <algorithm>

using namespace std;

class GUI
{
public:
    GUI();
    void start();
private:
    inputManagement InputManagement;
};

#endif // GUI_H


