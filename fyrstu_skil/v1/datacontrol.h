#ifndef DATACONTROL_H
#define DATACONTROL_H

#include <vector>
#include "Scientist.h"
#include <fstream>
#include <iostream>

class dataControl
{
private:
public:
    dataControl();
    void saveToTxt(std::vector<Scientist> scientistVector);//saves vector to txt
    std::vector<Scientist> readFile();//reads vector from file
    void writeFile(std::vector<Scientist> l);//writes vector to file

};

#endif // DATACONTROL_H

//comment
