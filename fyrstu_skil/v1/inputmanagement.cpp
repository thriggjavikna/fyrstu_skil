#include "inputmanagement.h"
#include "scientist.h"
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

inputManagement::inputManagement()
{
   // scientistVector = vector<Scientist>();
    dataControl dc;
    scientistVector = dc.readFile();
}

void inputManagement::add(Scientist legend) {
    scientistVector.push_back(legend);
}

ostream& operator<<(ostream& os, const Scientist& dude)
{
    if (!(dude.name.empty()))
        os << "\nScientist:" << "\nFirst name: " << dude.name << "\nLast name: " << dude.lastName << "\nGender: " << dude.gender << "\nBirthyear: " << dude.birthyear << "\nDeathyear: " << dude.deathyear << endl << endl;

    return os;
}


void inputManagement::display()//Displays out of vector
{

    cout << "Display scientists: " << endl;
    for (vector<Scientist>::iterator iter = scientistVector.begin(); iter!=scientistVector.end(); iter++) {
        cout << *iter;
    }
}

void inputManagement::saveAndExit()
{
    dataControl dc;
    dc.writeFile(scientistVector);
}

//Works but sort() will not work with the function.
bool operator< (Scientist &s1, Scientist &s2)
{
    if(s1.name != s2.name) return s1.name < s2.name;
    return s1.birthyear < s2.birthyear;

    return false;
}

bool inputManagement::sortStringAlphabeticallyByName(const Scientist &s1, const Scientist &s2)
{
    return &s1 < &s2;//s1 is further back in the alphabet.
}

void inputManagement::showSortedNames()
{

//    std::sort(scientistVector.begin(), scientistVector.end(),sortStringAlphabeticallyByName);
//    if(!(scientistVector[0].name < scientistVector[1].name))
         cout << "works";//Check works, sort wont accept.

}

void inputManagement::findString(string str)
{
    bool found = false;
    //iterate through and show matches
    for (unsigned int x = 0; x < scientistVector.size(); x++) {
        for(unsigned int y = 0; y < scientistVector[x].name.length(); y++)
        {
            //Loops through search string & compares to name
            if(((scientistVector[x].name[y] == str[y] && y != 0) && scientistVector[x].name[0] == str[0]) || (str.length() == 1 && scientistVector[x].name[0] == str[0]))
            {
                    cout << scientistVector[x];
                    found = true;
                    break;
            }
        }
    }
    if(!(found))
        cout << "No valid result.";

    while(true){//breaks out if valid input.
    cout << "\nGo back to Main menu?(1)\nchoice:";
    cin >> str;
    if(str == "1"){
        break;}
    else
        cout << "\nInvalid input.";
    }
}
