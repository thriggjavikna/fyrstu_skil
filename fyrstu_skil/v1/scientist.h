#ifndef SCIENTIST_H
#define SCIENTIST_H

#include <string>
using namespace std;

class Scientist
{
public:
    string name;
    string lastName;
    string gender;
    string birthyear;
    string deathyear;
    Scientist();
    friend bool operator< (Scientist &s1, Scientist &s2);
    friend ostream& operator<<(ostream& os, const Scientist& dude);
};

#endif // SCIENTIST_H

