#include "gui.h"

GUI::GUI()
{
    InputManagement = inputManagement();
}

void GUI::start() {
    int choice = 0;
    Scientist legend = Scientist();
    inputManagement Inout;
    cout << "Hello!\nThis is a program which lets you manage a list of people who were smarter than you.\n" << endl;

    while (choice != 1337){
    cout << "Which command would you like to use? Please enter a number." << endl;
    cout << "1: Add a person to the list." << endl;
    cout << "2: Search list." << endl;
    cout << "3: Display the list." << endl;
    cout << "4: Exit program and save list." << endl;

START:
    cin >> choice;
    if (cin.fail())
    {
        cin.clear();
        cin.ignore();
        cout << "Invalid input\n";
        cout << "Please re-enter selection. <1-4>\n";
        goto START;//goto4life
    }
    switch (choice)
        {
        case 1:
        {
            cout << "Enter scientist's name, gender, birth year and death year, seperated by pressing ENTER (Return)" << endl;
            cout << "First name:";
            //cin.ignore();
            //getline (cin,legend.name);
            cin >> legend.name;
            cout << "Last name: ";
            cin >> legend.lastName;
            cout << "Gender:";
            cin  >> legend.gender;
            cout << "Birth year:";
            cin >> legend.birthyear;
            cout << "Year/date of death:";
            cin >> legend.deathyear;
            Inout.add(legend);
            break;
        }
        case 2:
        {
            string str;
            cout << "Type in the name to search for:\n:";
            cin >> str;
            Inout.findString(str);
            break;
        }
        case 3:
        {
            Inout.display();
            break;
        }
        case 4:
        {
            Inout.saveAndExit();
            choice = 1337;//stop while loop.
            break;
        }

        default:
        {
            cout << "Invalid input\n";
            break;
        }
        }
    };
}

//comment
