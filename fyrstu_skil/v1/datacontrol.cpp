#include "datacontrol.h"

dataControl::dataControl()
{
}

//SaveToTxt
void dataControl::saveToTxt(std::vector<Scientist> scientistVector)
{
    writeFile(scientistVector);
}

//Read from file
std::vector<Scientist> dataControl::readFile()
{
    //from lecture
    // Read list from file and return as a vector
    std::vector<Scientist> v = std::vector<Scientist>();
    ifstream inFile ("data.txt");

    if(inFile.is_open()) {
        while(!inFile.eof()) {
            Scientist s = Scientist();
            inFile >> s.name >> s.lastName >> s.birthyear >> s.deathyear >> s.gender;
            v.push_back(s);

        }
        inFile.close();
    }
    return v;
}

//Write to file
void dataControl::writeFile(std::vector<Scientist> l)
{
    ofstream outFile ("data.txt");
    if(outFile.is_open()) {
        for(vector<Scientist>::iterator iter = l.begin(); iter != l.end(); iter++) {
            outFile << iter->name << endl << iter->lastName << endl << iter->birthyear << endl << iter->deathyear << endl << iter->gender << endl;
        }
        outFile.close();
    }
}
